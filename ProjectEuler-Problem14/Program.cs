﻿using System;
using System.Diagnostics;

namespace ProjectEuler_Problem14
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch watch = Stopwatch.StartNew();

            int inputSize = 1000000;
            
            CollatzSequence sequence = new CollatzSequence();
            var result = sequence.Calculate(inputSize);
            watch.Stop();

            Console.WriteLine("The longest sequence for input of {0} was {1} at number {2}", inputSize, result[0], result[1]);
            Console.WriteLine("Program ran for {0} seconds", watch.ElapsedMilliseconds/1000);

            Console.WriteLine("Press any key to exit the program");
            Console.ReadKey();
        }
    }
}
