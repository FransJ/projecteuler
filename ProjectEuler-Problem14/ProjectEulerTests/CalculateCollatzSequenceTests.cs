﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectEuler_Problem14;

namespace ProjectEulerTests
{
    [TestClass]
    public class CalculateCollatzSequenceTests
    {
        [TestInitialize]
        public void Initialize()
        {
            var bla = new CollatzSequence();
        }
        [TestMethod]
        public void InputValueOfTwoReturnsSequenceOfOne()
        {
            //Arrange
            var collatzSequence = new CollatzSequence();
            
            //Act
            var result = collatzSequence.Calculate(2);

            //Assert
            Assert.AreEqual(1,result[0]);
            Assert.AreEqual(2,result[1]);
        }
    }
}
