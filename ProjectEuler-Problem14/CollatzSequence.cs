﻿using System;

namespace ProjectEuler_Problem14
{
    public class CollatzSequence
    {
        private int _sequenceNumber;
        private int _maxSequenceSize;

        public int[] Calculate(int size)
        {
            for (int i = 2; i < size+1; i++)
            {
                Console.WriteLine(i);
                long sequenceNumber = i;
                int sequenceSize = 1;

                while (sequenceNumber != 1)
                {
                    if (sequenceNumber > 1)
                    {

                        if (sequenceNumber % 2 == 0)
                        {
                            sequenceSize++;
                            sequenceNumber = sequenceNumber / 2;
                        }
                        else
                        {
                            sequenceSize++;
                            sequenceNumber = sequenceNumber * 3 + 1;
                        }
                    }
                    else
                    {
                        sequenceSize++;
                        sequenceNumber = sequenceNumber / 2;
                    }
                }
                if (sequenceSize > _maxSequenceSize)
                {
                    _maxSequenceSize = sequenceSize;
                    _sequenceNumber = i;
                }
            }
            int[] result = { _maxSequenceSize, _sequenceNumber};
            return result;
        }
    }
}
